package com.caixy.adminSystem.config.properties;

/**
 * 登录类型
 *
 * @Author CAIXYPROMISE
 * @name com.caixy.adminSystem.config.properties.LoginType
 * @since 2024/10/28 01:37
 */
public enum LoginType {
    SESSION,
    TOKEN
}