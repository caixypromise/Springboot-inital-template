package com.caixy.adminSystem.controller;

import com.caixy.adminSystem.common.Result;
import com.caixy.adminSystem.common.ErrorCode;
import com.caixy.adminSystem.common.ResultUtils;
import com.caixy.adminSystem.exception.BusinessException;
import com.caixy.adminSystem.manager.Authorization.AuthManager;
import com.caixy.adminSystem.model.dto.postthumb.PostThumbAddRequest;
import com.caixy.adminSystem.model.vo.user.UserVO;
import com.caixy.adminSystem.service.PostThumbService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 帖子点赞接口
 */
@RestController
@RequestMapping("/post_thumb")
@Slf4j
public class PostThumbController
{

    @Resource
    private PostThumbService postThumbService;


    @Resource
    private AuthManager authManager;

    /**
     * 点赞 / 取消点赞
     *
     * @param postThumbAddRequest
     * @param request
     * @return resultNum 本次点赞变化数
     */
    @PostMapping("/")
    public Result<Integer> doThumb(@RequestBody PostThumbAddRequest postThumbAddRequest,
                                   HttpServletRequest request)
    {
        if (postThumbAddRequest == null || postThumbAddRequest.getPostId() <= 0)
        {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        // 登录才能点赞
        final UserVO loginUser = authManager.getLoginUser();
        long postId = postThumbAddRequest.getPostId();
        int result = postThumbService.doPostThumb(postId, loginUser);
        return ResultUtils.success(result);
    }

}
